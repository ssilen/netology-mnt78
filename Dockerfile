FROM centos:7

RUN yum update -y && yum install gcc openssl-devel bzip2-devel libffi-devel make wget -y
RUN wget -P /opt/ https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tgz && cd /opt/ && tar -xzf Python-3.8.1.tgz
RUN cd /opt/Python-3.8.1/ && ./configure --enable-optimizations && make altinstall
RUN curl https://bootstrap.pypa.io/get-pip.py -o /opt/get-pip.py && python3.8 /opt/get-pip.py
RUN pip3.8 install flask flask_restful flask_jsonpify
ADD python-api.py /opt/python-api.py

ENTRYPOINT ["python3.8", "/opt/python-api.py"]
